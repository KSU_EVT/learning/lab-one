# Lab 1: Basic Publish Subscribe

## Learning Outcomes

- Learn the basic building blocks for ros nodes
- Learn how to make a publisher for a certain data type
- Learn how to create a subscriber with a callback
- Learn how ros works over the network

## Prelab

Complete the installation instructions posted [here](https://gitlab.com/KSU_EVT/learning/software_setup)

## Lab Procedure

1. Choose  to work with either python or C++ the results will be the same but the syntax will vary a bit
2. Open a terminal in the folder corresponding to this  repository
3. Type ```catkin build``` and run
4. Type ```source devel/setup.bash```
5. Click the ROS core icon in the bottom left
6. Type the following and run ```rosrun lab_one chatter``` you should see some info logs
7. Open a new terminal and type ```rostopic echo /chatter``` you should see some hello worlds

### Challenge 1

You first challenge will be to change the publisher from publishing strings to publishing [std_msgs/Int16](http://docs.ros.org/api/std_msgs/html/msg/Int16.html) work on this with a  partner if you get stuck ask the lab instructor but make an effort to find the solution. You can test you code by using steps 3 and 4  to build and 6 and 7 to run and check.

### Challenge 2

Second we are gonna connect all of your publishers to a subscriber over the network make sure to shut down your executable from the last challenge.

1. Type ```hostname -I``` and write down your ip that begins with 10.100
2. Type ```export ROS_IP=YOURIP```
3. Type the ```export ROS_MASTER_URI=http://IP_PROVIDED_BY_INSTRUCTOR:11311```
